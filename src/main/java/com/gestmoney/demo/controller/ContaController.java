package com.gestmoney.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gestmoney.demo.models.ContaModel;
import com.gestmoney.demo.repository.ContaRepository;

import lombok.AllArgsConstructor;

import java.util.*;

@RestController
@AllArgsConstructor
public class ContaController {
    
    @Autowired
    ContaRepository repository;
    
    @GetMapping("/tabela_conta")
    public List<ContaModel> getAllconta(){
        return repository.findAll();
    }

    @GetMapping("/tabela_conta/{id}")
    public ContaModel getContaModelById(@PathVariable Long id){
        return repository.findById(id).get();
    }

    @PostMapping("/tabela_conta")
    public ContaModel saveConta(@RequestBody ContaModel conta){
        return repository.save(conta);
    }

    @DeleteMapping("/tabela_conta/{id}")
    public ResponseEntity<String> deleteContaModel(@PathVariable Long id){
        Optional<ContaModel> contaModelOp = repository.findById(id);
        if(!contaModelOp.isPresent() ){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ID invalido!!");
        }
        repository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Deletado com sucesso!!");
    }

    @PutMapping("/tabela_conta")
    public ContaModel atualizaContaModel(@RequestBody ContaModel conta){
        return repository.save(conta);
    }
}
