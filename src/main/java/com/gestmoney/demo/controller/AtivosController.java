package com.gestmoney.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gestmoney.demo.models.AtivosModel;
import com.gestmoney.demo.repository.AtivosRepository;

import lombok.AllArgsConstructor;

import java.util.*;

@RestController
@AllArgsConstructor
public class AtivosController {
    
    @Autowired
    AtivosRepository repository;
    
    @GetMapping("/tabela_ativos")
    public List<AtivosModel> getAllativos(){
        return repository.findAll();
    }

    @GetMapping("/tabela_ativos/{id}")
    public AtivosModel getAtivosModelById(@PathVariable Long id){
        return repository.findById(id).get();
    }

    @PostMapping("/tabela_ativos")
    public AtivosModel saveAtivos(@RequestBody AtivosModel ativos){
        return repository.save(ativos);
    }

    @DeleteMapping("/tabela_ativos/{id}")
    public void deleteAtivosModel(@PathVariable Long id){
        repository.deleteById(id);
    }
    
    @PutMapping("/tabela_ativos")
    public AtivosModel atualizaContaModel(@RequestBody AtivosModel conta){
        return repository.save(conta);
    }
}
