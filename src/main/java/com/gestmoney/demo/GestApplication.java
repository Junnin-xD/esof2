package com.gestmoney.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestApplication.class, args);
	}

}
