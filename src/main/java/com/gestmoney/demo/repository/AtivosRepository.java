package com.gestmoney.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gestmoney.demo.models.AtivosModel;

public interface AtivosRepository extends JpaRepository<AtivosModel, Long>{
    
}
