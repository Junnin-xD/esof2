package com.gestmoney.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gestmoney.demo.models.ContaModel;

public interface ContaRepository extends JpaRepository<ContaModel, Long>{
    
}
